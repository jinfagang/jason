//
// Created by robox on 17-11-8.
//

#include <fstream>
#include "../include/jason_parser.h"
#include "../include/errors.h"
#include "lib/t_os.h"

using namespace std;

JasonParser::JasonParser() {

}
JasonParser::~JasonParser() {

}

/// parse from file
/// \param file_path
/// \return
Json JasonParser::parse(std::string file_path) {
    // check if file exist
    ifstream ifs(file_path);

    string jsonStr = "";
    if (ifs) {
        while (!ifs.eof()) {
            string tmp;
            ifs >> tmp;
            jsonStr += tmp;
        }
        ifs.close();

        cout << "jsonStr: " << jsonStr << endl;

        Json json(jsonStr);
        return json;
    } else {
        throw FileNotFound("file: " + file_path + " not found");
    }

}

/// parse from a string
/// \param json_str
/// \return
Json JasonParser::parse_str(std::string json_str) {

}