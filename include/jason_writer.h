//
// Created by robox on 17-11-8.
//

#ifndef JASON_JASON_WRITER_H
#define JASON_JASON_WRITER_H

/// jason writer class write a STL container object into json.txt file
/// with indent and tab line options

class JasonWriter{
public:

    void write();
};
#endif //JASON_JASON_WRITER_H
