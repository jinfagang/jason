//
// Created by robox on 17-11-8.
//

#ifndef JASON_ERRORS_H
#define JASON_ERRORS_H

#include <iostream>
#include <string>


/// all errors in jason that might be happen

class FileNotFound{
private:
    std::string errorMessage;

public:
    FileNotFound(std::string msg);
    void error();
};
#endif //JASON_ERRORS_H
