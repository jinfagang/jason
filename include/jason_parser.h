//
// Created by robox on 17-11-8.
//

#ifndef JASON_PARSER_H
#define JASON_PARSER_H

/// jason parser class contains operation of parse a json object from
/// a file or a string
#include <iostream>
#include <string>
#include <vector>
#include "json.h"


class JasonParser{
public:
    JasonParser();
    ~JasonParser();

    Json parse(std::string file_path);
    Json parse_str(std::string json_str);


};

#endif
